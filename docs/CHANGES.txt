TraitsBackendWX 3.3.1 (not yet released)
========================================



TraitsBackendWX 3.3.0 (Feb 24, 2010)
====================================

Enhancements
------------


Fixes
-----



TraitsBackendWX 3.2.0 (July 15, 2009)
=====================================

Enhancements
------------

 * Reduced border_size from 1 to 4 for Group instances

 * Implemented editable_labels attribute in the TabularEditor for enabling editing of the labels (i.e. the first column)

 * Saving/restoring windows now works with multiple displays of different sizes

 * New ProgressDialog

 * Improved window colors to match desktop colors more closely

 * Changed default font to use system font

 * Improved support for opening links in external browser from HTMLEditor

 * Reduced the number of update events the PythonEditor fired

 * moved grid package from TraitsGui egg into enthought.pyface.ui.wx

 * moved clipboard from enthought.util.wx into pyface


Fixes
-----

 * Fixed bug in DateEditor which would not display Feb correctly if the current date was visible and greater than the number of days in Feb

 * Fixed layout issues:
   * Windows are now resized if they are larger than the desktop
   * Windows are now put in a valid location if they were opened off-screen
   * Windows with smaller parent are no longer opened at invalid positions with negative y values

 * Fix bug in ListEditor where a trait change listener would be fired when intermediate traits changed (when extended_name was of the type item1.item2.item3..) leading to a traceback.

 * Replaced calls of wx.Yield() with wx.GetApp().Yield(True)

 * Fixed TabularEditor compatibility problem with wx 2.6 regarding the page-down key

 * Fixed bug in propagating click events in the TabularEditor to parent windows

 * DateEditor wx 2.6 compatability fixed

 * TableEditor scrollbar fixed

